import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE \
    username = %(username)s"
buy_decrease_stock = (
    "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
)
add_items_to_player = "INSERT INTO Inventory (username, product, amount) VALUES \
    (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO \
    UPDATE SET amount = Inventory.amount + %(amount)s"
player_items_request = (
    "SELECT product, amount FROM Inventory WHERE username = %(username)s"
)
players_request = "SELECT * FROM PLayer"
shops_request = "SELECT * FROM Shop"
inventories_request = "SELECT * FROM Inventory"

MAX_ITEMS_PER_PLAYER = 100


def get_connection():
    return psycopg2.connect(
        dbname="test_db_1",
        user="postgres",
        password="this_should_be_properly_stored_password_but_its_not_because_its_a_quick_lab",
        host="postgres_container",
        port=5432,
    )  # TODO add your values here


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_stock, obj)

                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

        with conn.cursor() as cur:
            cur.execute(player_items_request, obj)
            owned_items = cur.fetchall()
            total_owned = sum(map(lambda item: item[1], owned_items))

        if amount + total_owned > MAX_ITEMS_PER_PLAYER:
            raise Exception(
                "Cannot fit {} items into {}'s inventory, only {} free slots left".format(
                    amount, username, MAX_ITEMS_PER_PLAYER - total_owned
                )
            )

        with conn.cursor() as cur:
            cur.execute(add_items_to_player, obj)


def print_all():
    def exec_select(request: str):
        with get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(request)
                print(cur.fetchall())

    print("Players:")
    exec_select(players_request)
    print()
    print("Shops:")
    exec_select(shops_request)
    print()
    print("Inventories:")
    exec_select(inventories_request)


print("Before:")
print_all()
print()

try:
    for i in range(99):
        buy_product("Alice", "water", 1)
    for i in range(2):
        buy_product("Alice", "marshmello", 1)
except Exception as e:
    print("Error during buy:", e)

print("After (last buy should fail and limit of 100 items should be reached):")
print_all()
print()
