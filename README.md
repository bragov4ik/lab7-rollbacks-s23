# Lab7 - Rollbacks and reliability

## Lab

For transactions I used built-in rollbacks on any exception in `psycopg2` library.
The action of buying an item is all done within one transaction context, with all necessary checks.
It means that in case of any exception, everything will be rolled back.

To make launch and setup easier I have created an initialization SQL script in `postgres_init_scripts`,
`Dockerfile` for the application, and `docker-compose.yml` for DB and pgadmin. To reset and launch the
compose(ition???) there's a script `restart_reset_compose.sh`, which I used in development.

## Results

The Python code contains showcase of buying function with necessary constraints. It tries to place
101 items in the inventory, which fails at the last one, but leaves the system in consistent state:

![program output](./output.png)
