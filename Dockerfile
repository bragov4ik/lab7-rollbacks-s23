FROM python:3.9.12-alpine AS builder
# musl-dev is a "general" C compiler necessary, from what I understood
RUN apk update && \
    apk add musl-dev libpq-dev gcc
# Create the virtual environment
RUN python -m venv /opt/venv
# Activate the virtual environment
ENV PATH="/opt/venv/bin:$PATH"

COPY requirements.txt .
RUN pip install -r requirements.txt

# Operational stage
FROM python:3.9.12-alpine

RUN apk update && \
    apk add libpq-dev

# Get the virtual environment from builder stage
COPY --from=builder /opt/venv /opt/venv

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PATH="/opt/venv/bin:$PATH"

WORKDIR /app

COPY . /app/

CMD [ "python", "./buy_product.py" ]
