DROP DATABASE IF EXISTS test_db_1;
CREATE DATABASE test_db_1;
\c test_db_1

CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
CREATE TABLE Inventory (username TEXT, product TEXT, amount INT CHECK(amount >= 0 AND amount <= 100));
CREATE UNIQUE INDEX idx_username_product_inventory on Inventory (username, product);

INSERT INTO Player (username, balance) VALUES ('Alice', 120);
INSERT INTO Player (username, balance) VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10), ('water', 100, 1);
